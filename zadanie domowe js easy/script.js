/* sprawdza czy x jest liczbą 
var x = '5';

if (!isNaN(x)) {
    console.log(true);
}else {
    console.log(false);
}
*/
/* sprawdza czy x jest liczbą całkowitą 
var x = '5';

if (Number.isInteger(x)) {
    console.log(true);
}else {
    console.log(false);
}
*/
/* sprawdza czy x jest stringiem 
var x = '5';

if (typeof x == 'string') {
    console.log(true);
}else {
    console.log(false);
}
*/
/* sprawdza czy x jest null'em. Jak za stringa 
'5' podstawię null to zwraca true (czy to jest dobrze??)
var x = '5';

if (x == null) {
    console.log(true);
}else {
    console.log(false);
}
*/
/* sprawdza czy x jest undefined (to samo pytanie 
jak wyżej??)
var x = undefined; 

if ( x === undefined) {
    console.log(true);
}else {
    console.log(false);
}
*/
/*
var i = 0;

while ( i < 100) {
    i++;
    if(i % 3 == 0)
    console.log(i);
}
*/

/* wszystkie liczby od 1 do 100 podzielne przez 3
przy użyciu pętli for 
for (i = 1; i <= 100; i++) {
    if (i % 3 == 0) {
        console.log(i)
    }

}
*/
 
/* kolejne elementy tablicy zawierają dni tygodnia
for (i = 1; i <= 7; i++) {
    switch(i) {
        case 1:
        console.log("pon");
        break;
        case 2:
        console.log("wt");
        break;
        case 3:
        console.log("śr");
        break;
        case 4:
        console.log("czw");
        break;
        case 5:
        console.log("pt");
        break;
        case 6:
        console.log("sob");
        break;
        case 7:
        console.log("nd");
        break;
    }

}
*/
/* wyniki z tabliczki mnożenia do 100
var tab = [];

for (i = 1; i <= 10; i++) {

    for (j = 1; j <= 10; j++) {
        console.log(i * j);
    }
    

}
*/
